
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(element => {
  element.style.backgroundColor = '#ff0000';
});

const byId = document.querySelector('#optionsList');
console.dir(byId);
const parentById = byId.previousElementSibling;
console.log(parentById);

const childrenById = [...byId.childNodes]
childrenById.forEach(node => {
  const res = `Type: ${node.nodeType}, name: ${node.nodeName}`;
  console.log(res);
});

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = 'This is a paragraph';

const mainHeaderChildren = document.querySelectorAll('.main-header > *');
mainHeaderChildren.forEach(child => {
  console.log(child);
  child.classList.add('nav-item')
});

const sections = document.querySelectorAll('.section-title');
sections.forEach(section => {
  section.classList.remove('section-title')
});

